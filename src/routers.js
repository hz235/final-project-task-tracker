import Welcome from './components/Welcome.vue'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Header from './components/Header.vue'
import {createRouter,createWebHistory} from 'vue-router'

const routes=[
    {
        name: 'Welcome',
        component: Welcome,
        path: '/'
    },    
    {
        name: 'Home',
        component: Home,
        path: '/home'
    },
    {
        name: 'Login',
        component: Login,
        path: '/login'
    },
    {
        name: 'Header',
        component: Header,
        path: '/home/header/v1.1'
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router;
