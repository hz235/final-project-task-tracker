const {MongoClient} = require('mongodb');

const uri = "mongodb://localhost:27017"
connect();
async function connect(){
    const client = new MongoClient();
    try{
        await client.connect();
        const db  = client.db("UserInfo")

        const users = db.collections("UserInfo");
        const cursor = await users.find();
    }
    catch(ex){
        console.error('Error fetching database ${ex}')
    }
    finally{
        client.close();
    }
}
