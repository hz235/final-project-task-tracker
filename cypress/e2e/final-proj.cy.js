
describe('The welcome page', () => {
  const username = 'admin';
  const pass = 'admin';
  it('Home page', () => {    
    cy.visit('http://localhost:8080')
    cy.contains('button', 'Go to login page')
    cy.get('button').click()
    cy.get('[type="text"]').type(username)
    cy.get('[type="password"]').type(pass)
  })
  it('Login Page', ()=> {
    cy.visit('http://localhost:8080/login')
    
    cy.get('[type="text"]').type(username)
    cy.get('[type="password"]').type(pass)
    cy.get('#login').click();
  })

  it('Home Page Adding Task', () => {
    cy.visit('http://localhost:8080/home/header/v1.1')
    cy.get(':nth-child(1) > input').type('test-task')
    cy.get('.add-form > :nth-child(2) > input').type('02/10/1998')
    cy.get(':nth-child(3) > input').type('Zhang')
    cy.get(':nth-child(4) > input').type('High')
    cy.get('.form-control-check > input').check()
    cy.get('.btn').click()
    cy.get(':nth-child(6) > .task').should('be.visible')
    cy.get(':nth-child(6) > .task').contains('test-task').contains('Zhang')
    cy.get(':nth-child(6) > .task > p').contains('02/10/1998')
    cy.get(':nth-child(6) > .task > h3 > .fas').click()
    cy.get(':nth-child(6) > .task').should('not.exist');

    cy.get(':nth-child(3) > input').type('Zhang')
    cy.get(':nth-child(4) > input').type('High')
    cy.get('.btn').click()
    cy.get(':nth-child(6) > .task').should('not.exist');
  })

  it('Home Page Adding Empty Task', () => {
    cy.visit('http://localhost:8080/home/header/v1.1')
    cy.get(':nth-child(3) > input').type('Zhang')
    cy.get(':nth-child(4) > input').type('High')
    cy.get('.btn').click()
    cy.get(':nth-child(6) > .task').should('not.exist');
  })

  it('Log Out', () => {
    cy.visit('http://localhost:8080/home/header/v1.1')
    cy.get('a').click()
    
  })
  
})